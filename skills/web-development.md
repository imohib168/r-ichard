---
name: "Web development"
level: 3
---
I spent most of my career developing, designing, and auditing web applications.
Web development is the focus of most of my work, including digital signage applications, project management, and e-commerce solutions.

I’ve worked for more than 8 years with HTML, CSS, PHP (Symfony), and Javascript (React, GraphQL).
Most recently, I've built an API with Python (Django).

I have a keen interest in web development and an eagerness to try new things.
