---
thumbnail: /images/aliceperchaud.png
illustration: /images/alice_gros.png
project_title: "aliceperchaud.fr"
sub_title: "Gatsby based website and blog"
tldr: "Setting up a custom Gatsby website to allow Alice to publish articles and enhance SEO score"
skills:
- Web development
- Project management
- DevOps
challenges:
- Learning to use Gatsby CMS to solve complex issues
- Adapting a bootstrap theme into React Gatsby website matching Alice's color scheme
- Migration from WordPress to Gatbsy with NetlifyCMS
---

The main goal was to create a more modern website and enhance performances and the general UX/UI.

The project was planned through many incremental deadlines. The progressive implementation of tools allowed Alice to benefit from her new website features quickly.

The first step was to create a whole new identity design and match it with an existing bootstrap theme to avoid spending too much time creating one from scratch.

Then we had to migrate all the data. Alice was previously working on a WordPress website that had a few articles published.
With the help of a WordPress plugin, we managed to export everything to markdown files and use NetlifyCMS instead.
We set up redirections for all the articles to match the new site sitemap. 

Finally, we set up every specific page that Alice needed and added some external tools such as Google Analytics and hotjar, essential when working as a marketing consultant.
