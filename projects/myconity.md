---
thumbnail: /images/myconity.png
project_title: "myconity.fr"
sub_title: "Web + mobile application with Symfony and React Native"
tldr: "Web application development and maintenance to help owners and builders follow house building projects and management of the mobile application development"
skills:
- Web development
- Team management
- Database management
- DevOps
- Cloud Architecture
challenges:
- Cleaning and testing legacy code for maintainability
- Driving a development agency to create a mobile App
- Working and coordinating with a freelancer to set up an API
---

MyConity is an application for home builders and their customers. It allows users to follow the progress of a project
step-by-step, from the request for a building permit to the handover of the keys.

Its main functions are:

- collecting reviews
- uploading files
- electronic signatures
- exchanging photos between the builder and the buyer
- monitoring email

I arrived at CONITY shortly after the creation of the first "MyCONITY" product. My first task was to internalize the
whole solution and take possession of the code. I was able to audit, structure, and strengthen the solution code.

During this period, I had to set up the cloud architecture that would host the solution. I trained myself to the
different AWS services (EC2, RDS, CloudFront, ElasticBeanstalk, etc.) to create a robust and scalable structure per the
company's vision.

After that, we defined tests cases to ensure the stability of the application. Then we started working with a freelancer
and a development agency to build an API and a mobile application.

My last mission on MyCONITY was to create bridges with other industry partner's applications by sharing data through our
API.
