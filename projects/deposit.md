---
thumbnail: /images/deposit.png
illustration: /images/deposit_detail.png
project_title: "Deposit"
sub_title: "Web platform connected with banking API with enhanced UI/UX"
tldr: "Setting up a house delivery platform to handle banking operations for builders and owners"
skills: 
 - Web development
 - Database management
 - Team management
 - Project management
 - DevOps 
challenges:
 - Connecting to Lemonway API 
 - Creating a reassuring UI/UX
 - Setting up several automatic emails 
---

Deposit is a web application built to help owners at the delivery state of a house building project.
Conity's motivation with Deposit was to simplify the delivery of a house. To do so, the owner needs to feel safe to pay the last part of the bill without being afraid of a small crack on the wall or minor missing paints.

With the help of Lemonway, we set up an application where the client will transfer money to a temporary secured account for future bill payment.

The main concern was about trust, so we worked with a freelancer to set up a clear and reassuring interface for users. They needed to know what they were doing and feel safe to provide all the legal information that allowed us to create a wallet via Lemonway API.

We also had to set up several automatic emails to keep every actor updated with the delivery process.
To do so, we used SendGrid and multiple queues to scale quickly.
