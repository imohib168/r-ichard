---
thumbnail: /images/integralsystem.png
illustration: /images/integral_big.png
project_title: "integral-system.fr"
sub_title: "A custom E-commerce solution"
tldr: "Using Sylius framework we have set up a custom E-commerce solution connected to internal ERP"
skills:
- Web development
- Database management
challenges:
- Understanding Sylius principles
- Connecting the solution to internal tools
- Setting up search engine via Elastic Search
---

I was part of the development team on this project, and it's one of my first Symfony and Sylius projects.

The goal is to use a highly reliable framework to create a new e-commerce website for Integral-System.

This application needed to be linked to the internal ERP to show products’ availability in stock.

Most of my work was setting up the search engine with Elastic Search to allow faster and reliable search capabilities on hundreds of products.
