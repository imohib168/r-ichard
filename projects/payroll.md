---
thumbnail: /images/payroll.png
project_title: "Payroll automation"
sub_title: "Internal tools + performance reporting website"
tldr: "Setting up, assisting and maintaining internal payroll automating tools + Building a decision support tool the asses performances of payroll automation"
skills:
- Web development
- Database management
challenges:
- Understanding business needs
- Working with VBA
- Assisting accountant daily on any issues with the internal tools
---

Working at Adecco France was my first job in the IT field. My task there was to assist the regional customer automation manager with his chores. I worked in a service center with accountants and helped them set up internal tools to automate payroll.
I worked a lot on Excel with a custom VBA code that exported timesheets into an internal software.

Later on, I had to set up a decision support tool for regional managers to help them get a broad view of automation tools' deployment and efficiency.
This tool aggregated raw data from BI exports to create charts and tables highlighting the key performance indicators.
