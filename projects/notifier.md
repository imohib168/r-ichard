---
thumbnail: /images/notif.png
project_title: "Notification manager"
sub_title: "Lambda function with Amazon SQS to simplify notifications"
tldr: "Building a multi-channel notification dispatcher using SQS and Lambda function"
skills:
- Web development
- Cloud Architecture
- DevOps 
challenges:
- Getting to know Lambda and SQS
- Creating a multi-channel dispatcher in Python
- Writing documentation and tutorials for the development team

---

This project is proof of concept that I've set up for Conity's needs. The idea was simple: To create a microservice that
will handle any kind of notification to any web application user.

After some research, the easiest way to handle this kind of notification in a scalable way was to use Amazon SQS (Simple
Queue Service) and Lambda function.

All our applications can call Amazon SQS (via API) and add a notification to the queue. Once the notification is in the
queue, a Lambda function will be triggered and will dispatch this notification
via [Apprise](https://github.com/caronc/apprise) Python library to the required channel.

This allows a vast amount of notifications to be dispatched in a wide variety of channels (SMS, Slack notification,
Push, Email, etc.)
