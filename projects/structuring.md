---
thumbnail: /images/structuring.png
project_title: "Structuring IT solutions"
sub_title: "Benchmarking and action planning for Conity's internal tools"
tldr: "Helping the team to find ideal tools that suited its needs"
skills:
- Team management
- Business analysis
challenges:
- Understanding everyone needs and work methodology
- Documenting existing processes 
- Benchmarking multiple tools and presenting them to coworkers 
---

During my time at CONITY, many internal solutions (organization, team management, accounting, and knowledge management) had to be applied and organized.

For example, the implementation of Google Workplace simplified document sharing, collaboration, and internal communication.

We also implemented Slab to allow people to find all the company's processes and documentation.

Setting up and helping everyone get the tools they needed is one of the tasks I enjoyed the most at Conity.
