---
thumbnail: /images/conitytips.png
project_title: "Conity's tips"
sub_title: "Managing several blogs with git-based CMS"
tldr: "Architecture and implementation of a version management oriented CMS to ease updates of advice articles helping purchasers"
skills:
- Web development
- DevOps
- Business analysis
challenges:
- Understanding the business need and finding a perfectly suited solution
- Training colleagues to understand the principle of a git tree 
- Migrating all the existing articles from Word/PDF files to markdown files
---

Conity publishes a series of blog posts with tips for home buyers. The client can choose to use this content like a white brand or not.  

Due to the growth of the customer base, updating and populating these tips has become complex and tedious.
To address this issue, I simplified the process of updating this knowledge base.

The final architecture of the solution is based on the concept of branching that Git allows.
