---
thumbnail: /images/car.png
illustration: /images/car_big.png
project_title: "Smart car DIY"
sub_title: "Building and programming on an arduino based car"
tldr: "Personal project of developing algorithm to allow a smart car to perform tricks"
skills:
- Embedded | IoT
- Robotics
challenges:
- Coding in C for Arduino
- Understanding how each component work within the car 
- Electronic assembly
---

Since I am passionate about robotics, I enjoyed working on this weekend-long project. It allowed me to learn about C programming to help this little car avoid obstacles and follow a line.

This project has expanded my knowledge of the C language on the Arduino board.

I enjoyed assembling electronic components to get a tangible result and how it contrasted with my daily web development work.
