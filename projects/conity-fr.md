---
thumbnail: /images/conity.png
project_title: "conity.fr"
sub_title: "BtoC multi-purpose website"
tldr: "Design and maintenance of a showcase site, implementation of a CMS and a review of administration space."
skills:
- Web development
- Database management
- Team management
- DevOps
challenges:
- Coordinating internal and external teams
- Adding a CMS in a short amount of time
---
conity.fr is CONITY's showcase website used to promote their products (MyCONITY, Deposit, Batysign).
This site is connected to MyCONITY API to display and manage buyer’s reviews and projects.

This has been one of the most challenging projects for me. The main challenge was coordinating both external and internal teams to develop the different parts of the website.
It was also necessary to quickly link a headless CMS to conity.fr to allow the internal team to publish articles that met SEO goals.

The CMS part was done with Strapi and MongoDB, an adequate solution for Conity back then.

