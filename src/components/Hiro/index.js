import * as React from "react"
import {hiroSection, hiroTitle,  particleContainer, hiroText} from "./style.module.scss"
import {row, ctaLink} from "../Layout/layout.module.scss"
import "@fontsource/roboto/900.css"
import "@fontsource/roboto-mono/700.css"
import Particles from "react-particles-js"
import {useEffect, useState} from "react";

const Hiro = () => {
    let particleColor = "#5485B6"
    const [isDesktop, setDesktop] = useState(false)

    useEffect(() => {
        if (typeof window !== "undefined") {
            setDesktop(window.innerWidth > 768)
        }
    }, [])

    const updateMedia = () => {
        setDesktop(window.innerWidth > 768)
    }

    useEffect(() => {
        window.addEventListener("resize", updateMedia)
        return () => window.removeEventListener("resize", updateMedia)
    })
    return (
        <section className={`${row} ${hiroSection}`}>
            <div className={hiroText}>
              <span className={hiroTitle}>
                Richard Raduly,
                <h1>Technical Lead</h1>
              </span>
                <p>
                    8+ years experience - Innovation enthusiast, loving technical challenges.<br/>

                </p>
                <a className={ctaLink} href="#about">
                    +&nbsp;More info
                </a>
            </div>
            <div className={`${particleContainer}`}>
                {isDesktop ? (<Particles width="37vw" height="80vh" params={{
                    "particles": {
                        "color": {
                            value: `${particleColor}`
                        },
                        "number": {
                            "value": 70
                        },
                        "size": {
                            "value": 3
                        },
                        "anim": {
                            "random": true
                        },
                        "line_linked": {
                            "enable": true,
                            "color": `${particleColor}`
                        },
                        "move": {
                            "speed": 0.5
                        }
                    },
                    "interactivity": {
                        "detect_on": "canvas",
                        "events": {
                            "onhover": {
                                "enable": true,
                                "mode": "bubble"
                            },
                            "onclick": {
                                "enable": true,
                                "mode": "repulse"
                            },

                            "resize": true
                        },
                        "modes": {
                            "bubble": {
                                "distance": 200,
                                "size": 4,
                                "duration": 2,
                                "opacity": 1,
                                "speed": 3
                            },
                            "repulse": {
                                "distance": 250,
                                "duration": 0.2
                            },
                        }
                    }
                }}/>) : ""}
            </div>

        </section>
    )
}


export default Hiro
