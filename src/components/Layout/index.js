import * as React from "react"
import PropTypes from "prop-types"
import "@fontsource/roboto"
import "@fontsource/roboto-mono/700.css"
import "normalize.css"
import "./theme.scss"
import {pageWrapper} from "./layout.module.scss"
import Navbar from "../Navbar"

const Layout = ({children}) => {
    return (
        <>
            <div>
                <Navbar/>
                <div className={pageWrapper}>
                    {children}
                </div>
            </div>
        </>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Layout
