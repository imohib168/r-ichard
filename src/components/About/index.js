import * as React from "react"
import {bigSubTitle, blue, grey, row, column, title, ctaLink} from "../Layout/layout.module.scss"
import {
    aboutSection,
    aboutEmail,
    aboutRight,
} from "./style.module.scss"
import "@fontsource/roboto-mono/700.css"
import downloadFile from '../../files/resume.pdf'


class About extends React.Component {
    render() {

        return (
            <section id="about" className={`${row} ${aboutSection}`}>
                <h2 className={title}><strong>_About</strong></h2>
                <div className={bigSubTitle}>Richard Raduly</div>
                <h3 className={`${blue}`}>Technical Lead</h3>
                <a className={`${aboutEmail} default-color`} href="mailto:hello@r-ichard.com">hello@r-ichard.com</a>
                <div className={`${row}`}>
                    <div className={`${column}`}>
                        <p>
                            <a href="https://www.linkedin.com/in/r-ichard/" target="_blank"
                               rel="noreferrer">linkedin</a><strong className={grey}>_</strong>
                            <a href="https://github.com/r-ichard" target="_blank" rel="noreferrer">github</a><strong
                            className={grey}>_</strong>
                            <a href="https://twitter.com/richard0r" target="_blank" rel="noreferrer">twitter</a>
                        </p>
                        <p>
                            I’ve been passionate about computer science and innovation since I was 16 years old. With a
                            BS in computer science, I had the opportunity to work for companies of various sizes. For
                            the last 3 years, I have held the position of CTO in a French start-up. This period was very
                            constructive for me and deep-rooted my motivation to work in IT project management.
                        </p>

                    </div>
                    <div className={`${column} ${aboutRight}`}>
                        <div>
                            <span>
                                <svg width="80" height="65" viewBox="0 0 80 65" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M33.6696 60.919C38.6638 57.5875 42.7249 53.0363 45.4685 47.6964C48.212 42.3565 49.5472 36.4047 49.3475 30.4046C49.1478 24.4044 47.4199 18.5546 44.3274 13.4089C41.2348 8.26327 36.88 3.99215 31.6754 1V1C24.9595 4.83311 19.6913 10.7693 16.6832 17.893C13.6752 25.0167 13.0944 32.9322 15.0305 40.4187L15.3362 41.5487C15.7617 42.9845 20.561 58.3532 32.4066 60.826C34.7377 61.2813 37.1349 61.2813 39.4661 60.826C49.9821 59.0445 55.9115 50.4163 57.5202 47.877L58.052 47.0261C60.8115 42.3462 62.2625 37.0106 62.2531 31.5777C62.2531 23.468 59.0315 15.6904 53.2971 9.95601C47.5627 4.22157 39.7851 1 31.6754 1C23.5657 1 15.7881 4.22157 10.0537 9.95601C4.31924 15.6904 1.09766 23.468 1.09766 31.5777C1.09587 39.0901 3.85964 46.3403 8.86175 51.9451"
                                            stroke="#5485B6" strokeWidth="2" strokeLinecap="round"
                                            strokeLinejoin="round"/>
                                        <path d="M4.63477 17.2993C22.1459 23.4016 41.2063 23.4016 58.7175 17.2993"
                                              stroke="#5485B6" strokeWidth="2"
                                              strokeMiterlimit="10" strokeLinecap="round"/>
                                        <path
                                            d="M1.73633 37.8262C21.2091 43.9438 42.0895 43.9438 61.5623 37.8262V37.8262"
                                            stroke="#5485B6" strokeWidth="2"
                                            strokeMiterlimit="10" strokeLinecap="round"/>
                                </svg>
                            </span>
                            <p>
                                Proven ability to lead a project while taking into account all aspects of it, from
                                technical architecture to profitability
                            </p>
                        </div>
                        <div>
                            <span>
                                <svg width="80" height="65" viewBox="0 0 80 65" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M30.6759 60.469C46.8346 60.469 59.9338 47.3698 59.9338 31.2111C59.9338 15.0523 46.8346 1.95312 30.6759 1.95312C14.5172 1.95312 1.41797 15.0523 1.41797 31.2111C1.41797 47.3698 14.5172 60.469 30.6759 60.469Z"
                                        stroke="#5485B6" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"/>
                                    <path
                                        d="M30.6772 10.4639C42.1144 10.4639 51.4237 19.7732 51.4237 31.2104C51.4237 42.6476 42.1144 51.9569 30.6772 51.9569C19.24 51.9569 9.93066 42.6476 9.93066 31.2104"
                                        stroke="#5485B6" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"/>
                                    <path
                                        d="M30.6764 42.382C24.5589 42.382 19.2393 37.3283 19.2393 30.9448C19.2393 24.8272 24.2929 19.5076 30.6764 19.5076C36.794 19.5076 42.1136 24.5612 42.1136 30.9448"
                                        stroke="#5485B6" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"/>
                                </svg>

                            </span>
                            <p>
                                8+ years working on building web applications
                            </p>
                        </div>
                        <div>
                            <span>
                                <svg width="80" height="65" viewBox="0 0 80 65" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M23.8548 3.92896C34.228 -1.12468 46.9951 0.73719 55.7725 9.51457L64.6346 18.3767C67.8196 21.5616 67.8196 26.7254 64.6346 29.9104L55.7725 38.7725C44.8673 49.6777 27.0465 49.6777 16.1413 38.7725L1.77832 24.1435"
                                        stroke="#5485B6" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"/>
                                    <path
                                        d="M36.0891 40.3679C45.0498 40.3679 52.314 33.1038 52.314 24.1431C52.314 15.1823 45.0498 7.91821 36.0891 7.91821C27.1284 7.91821 19.8643 15.1823 19.8643 24.1431C19.8643 33.1038 27.1284 40.3679 36.0891 40.3679Z"
                                        stroke="#5485B6" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round"/>
                                </svg>


                            </span>
                            <p>Looking to expand knowledge by joining a technically challenging company</p>
                        </div>
                    </div>
                </div>
                <div className={row}>
                    <a className={ctaLink} href={downloadFile} download>+ Download resume</a>
                </div>
            </section>
        )
    }
}

export default About
