import * as React from "react"
import {
    skillsList,
    skillsSelect,
    selectContainer,
    levelDescription,
    activeColor,
    selectedTagContainer,
} from "./style.module.scss"
import {row, mb3} from "../Layout/layout.module.scss"
import "@fontsource/roboto/900.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {useState} from "react";
import {HTMLContent} from "../Content";

const Skills = (props) => {

    const {skills, sliderRef, skillFilter, setSkillFilter} = props


    const mapping = [
        'beginner',
        'medium',
        'advanced',
        'passionate',
    ]

    const [isBeginnerActive, setBeginnerActive] = useState(false)
    const [isMediumActive, setMediumActive] = useState(false)
    const [isAdvancedActive, setAdvancedActive] = useState(false)
    const [isPassionateActive, setPassionateActive] = useState(false)


    const allStates = {
        'beginner': isBeginnerActive,
        'medium': isMediumActive,
        'advanced': isAdvancedActive,
        'passionate': isPassionateActive
    }

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    const LevelLabel = (props) => {
        let fnName = 'set' + capitalize(props.level) + 'Active'

        return (
            <span className={props.level + "-level level-label"}
                  role="link"
                  onMouseOver={() => {
                      eval(fnName + '(true)')
                  }}
                  onFocus={() => {
                      eval(fnName + '(true)')
                  }}
                  onMouseLeave={() => {
                      eval(fnName + '(false)')
                  }}>
                {props.level}
                </span>
        )
    }

    const handleSkillClick = (skill) => {
        if (skill === skillFilter) {
            setSkillFilter(null)
        } else {
            setSkillFilter(skill)
        }
        sliderRef.current.slickGoTo(0)
    }

    const handleSkillSelect = (event) => {
        if(event.target.value == "0"){
            setSkillFilter(null)
        } else {
            setSkillFilter(event.target.value)
        }
        sliderRef.current.slickGoTo(0)
    }

    return (
        <>
            <h4>Pick a skill to filter:</h4>

            <div className={`${row} ${skillsSelect}`}>
                <div className={selectContainer}>
                    <select name="skills" id="skills"
                            onChange={handleSkillSelect}
                    >
                        <option value="0">
                            All skills
                        </option>
                        {skills && skills.map((skill, idx) => (
                            <option key={idx} value={skill.frontmatter.name}>
                                {skill.frontmatter.name}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
            <div className={`${row} ${skillsList} `}>
                {skills && skills.map((skill, idx) => (
                    <button key={idx}
                            onClick={() => {
                                handleSkillClick(skill.frontmatter.name)
                            }}
                            className={skillFilter === skill.frontmatter.name ? 'active' : ''}
                    >
                        <span
                            className={`${mapping[(skill.frontmatter.level - 1)]}-color ${allStates[`${mapping[(skill.frontmatter.level - 1)]}`] || skillFilter === skill.frontmatter.name ? activeColor : null}`}
                        >+</span>&nbsp;
                        {skill.frontmatter.name}
                    </button>
                ))}
            </div>
            <div id="project" className={`${row} ${selectedTagContainer}`}>
                {skillFilter && skills && skills.map((skill, idx) => {
                    if (skillFilter === skill.frontmatter.name) {
                        return <div key={idx}>
                            <div className={`${row}`}><h4>{skill.frontmatter.name} </h4> <LevelLabel
                                level={mapping[(skill.frontmatter.level - 1)]}/></div>
                            <HTMLContent className={`${row} ${levelDescription}`} content={skill.html}/>
                        </div>
                    }
                    return <i key={idx}></i>
                })}
                {skillFilter === null ?
                    <>
                        <h4 className={`${row}`}>You can filter my projects by skills</h4>
                        <p>
                            Each skill has a level of expertise indicator:
                        </p>
                        <div className={`${row} ${mb3}`}>
                            {mapping && mapping.map((level, idx) => {
                                return <LevelLabel key={idx} level={level}/>
                            })}
                        </div>
                    </> : ''
                }
            </div>
        </>
    )
}


export default Skills
