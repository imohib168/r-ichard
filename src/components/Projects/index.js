import * as React from "react"
import {
    projectsSection,
    projectsThumbnails,
    projectThumb,
    overlay,
    backgroundImage,
    projectDetail,
    projectDescription,
    projectSkills,
    challengesDescription,
    projectHeader,
    projectIntro,
    sliderNavigator,
    buttonWrapper,
    closeButton,
} from "./style.module.scss"
import _ from 'lodash'
import {row, column, title, bigSubTitle, blue} from "../Layout/layout.module.scss"
import "@fontsource/roboto/900.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {useRef, useState} from "react";
import Slider from "react-slick";
import {HTMLContent} from "../Content";
import {navigate} from "gatsby";
import Skills from "./skills";
import {GatsbyImage, getImage} from 'gatsby-plugin-image'


const Projects = (props) => {
    const {data} = props
    let projects = []
    let skills = []
    const [skillFilter, setSkillFilter] = useState(null)


    let sliderSettings = {
        dots: false,
        adaptiveHeight: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };
    const sliderRef = useRef();


    data.forEach(element => {
        if (_.get(element, `frontmatter.project_title`)) {
            projects = projects.concat(element)
        } else if (_.get(element, `frontmatter.level`)) {
            skills = skills.concat(element)
        }
    })
    skills = _.uniq(skills)

    projects = _.uniq(projects)

    const handleThumbnailCLick = (index) => {
        navigate("#project")
        sliderRef.current.slickGoTo(index, false)
    }

    const ProjectThumbnail = (props) => {
        const image = getImage(props.thumbnail)
        return <div
                    className={projectThumb}
                    onClick={() => {
                        handleThumbnailCLick(props.idx + 1)
                    }}>
            <GatsbyImage alt={"thumbnail for "+props.title} image={image} height="auto" className={backgroundImage}/>
            <div className={overlay}>
                <h5>{props.title}</h5>
                <h6>{props.sub_title}</h6>
            </div>
        </div>
    }

    const ProjectDetail = (props) => {
        const illustration = getImage(props.illustration)
        return (
            <div className={projectDetail}>

                <div className={`${row} ${projectHeader}`}>
                    <h5>{props.title}</h5>
                    <div className={`${sliderNavigator}`}>
                        <div className={buttonWrapper}>
                            <button onClick={() => {
                                sliderRef.current.slickPrev()
                            }}>
                                <svg width="24" height="23" viewBox="0 0 24 23" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.0171 20.8181L2.28809 11.0891L12.0171 1.66811"
                                          stroke="#5485B6"
                                          strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                                </svg>
                            </button>
                            <button onClick={() => {
                                sliderRef.current.slickNext()
                            }}>
                                <svg width="24" height="23" viewBox="0 0 24 23" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.9575 1.51406L21.6865 11.243L11.9575 20.664"
                                          stroke="#5485B6"
                                          strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                                </svg>
                            </button>
                            <button onClick={() => {
                                sliderRef.current.slickGoTo(0)
                            }} className={closeButton}>
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.52637 2.13245L17.7477 18.3538" stroke="#5485B6"
                                          strokeWidth="2"
                                          strokeLinecap="round"/>
                                    <path d="M17.749 2.13232L1.52768 18.3537" stroke="#5485B6"
                                          strokeWidth="2"
                                          strokeLinecap="round"/>
                                </svg>
                                <span>Close project</span>
                            </button>
                        </div>
                    </div>

                </div>
                <div className={`${row}`}>
                    <div className={`${column}`}>
                        <div className={projectIntro}>

                            <h6>{props.sub_title}</h6>
                            <p><span className="reversed-colors">
                                            <strong>TLDR;</strong>&nbsp;{props.tldr}</span></p>
                        </div>
                        <div className={projectDescription}>
                            <strong>Description</strong>
                            <HTMLContent content={props.content}/>
                        </div>
                        <div className={challengesDescription}>
                            <strong>Challenges</strong>
                            <ul>
                                {props.challenges && props.challenges.map((challenge, idx) => (
                                    <li key={idx}>&nbsp;+&nbsp;{challenge}</li>
                                ))}
                            </ul>
                        </div>
                    </div>
                    <div className={`${column}`}>

                        <div className={`${row} ${projectSkills}`}>
                            <strong>Skills:</strong>
                            <div>
                                {props.skills && props.skills.map((skill, idx) => (
                                    <span key={idx}>&nbsp;+&nbsp;{skill}</span>
                                ))}
                            </div>

                        </div>

                        <div className="illustration">
                            {typeof illustration !== 'undefined' ?
                                <div className="img-placeholder" >

                                        <GatsbyImage alt={"illustration for "+props.title} image={illustration} />

                                </div>:null
                            }

                        </div>
                    </div>

                </div>
            </div>)
    }

    return (
        <section id="projects" className={`${row} ${projectsSection}`}>
            <h2 className={title}>_Projects</h2>
            <div className={bigSubTitle}>What I've <span className={blue}>done</span></div>
            <Skills skills={skills} sliderRef={sliderRef} skillFilter={skillFilter} setSkillFilter={setSkillFilter}/>

            <Slider {...sliderSettings} ref={sliderRef}>
                <div className={`${row}`}>
                    <div className={`${projectsThumbnails}`}>
                        {
                            projects && projects.map((project, idx) => {
                                if (skillFilter === null || (project.frontmatter.skills && project.frontmatter.skills.indexOf(skillFilter) >= 0)) {
                                    return <ProjectThumbnail key={idx}
                                                             idx={idx}
                                                             thumbnail={project.frontmatter.thumbnail}
                                                             title={project.frontmatter.project_title}
                                                             sub_title={project.frontmatter.sub_title}
                                    />
                                } else {
                                    return ''
                                }
                            })
                        }
                        <div
                            role="link"
                            tabIndex={-999}
                            className={projectThumb}
                            onClick={() => {
                                navigate('#contact')
                            }}>

                            <img alt={"illustration for cta"} src="/images/lets.webp" className={backgroundImage}/>
                            <div className={overlay}>
                                <h5>Let's fill this project together</h5>
                                <h6>I'm looking forward to get new missions. Get in touch !</h6>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    projects && projects.map((project, idx) => {
                        return <ProjectDetail
                            key={idx}
                            title={project.frontmatter.project_title}
                            sub_title={project.frontmatter.sub_title}
                            illustration={project.frontmatter.illustration}
                            tldr={project.frontmatter.tldr}
                            challenges={project.frontmatter.challenges}
                            skills={project.frontmatter.skills}
                            content={project.html}
                        />

                    })
                }
            </Slider>
        </section>
    )
}


export default Projects
