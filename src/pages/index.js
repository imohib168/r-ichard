import * as React from "react"
import Layout from "../components/Layout"
import Seo from "../components/SEO"
import Hiro from "../components/Hiro";
import Projects from "../components/Projects";
import {graphql} from "gatsby";
import About from "../components/About";
import Contact from "../components/Contact";
import Footer from "../components/Footer";

const IndexPage = (props) => {
    const data = props.data
    const nodes = data.allMarkdownRemark.nodes

    return (
        <>
            <Seo/>
            <Layout>
                <Hiro/>
                <Projects data={nodes}/>
                <About/>
                <Contact/>

            </Layout>
            <Footer/>
        </>
    )
}
export const query = graphql`
    query PageQuery {
        allMarkdownRemark {
            nodes {
                id
                html
                frontmatter {
                    thumbnail {
                        childImageSharp {
                            gatsbyImageData(layout: FULL_WIDTH)
                        }
                    }
                    illustration {
                        childImageSharp {
                            gatsbyImageData(width: 600, layout: CONSTRAINED)
                        }
                    }
                    project_title
                    tldr
                    sub_title
                    skills
                    challenges
                    name
                    level
                }
            }
        }
    }
`
export default IndexPage
